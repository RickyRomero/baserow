export default {
  user: {
    isStaff: 'Is staff',
    isGroupAdmin: 'Is group admin',
    active: 'Active',
    deactivated: 'Deactivated',
  },
  adminType: {
    dashboard: 'Dashboard',
    users: 'Users',
    groups: 'Groups',
    licenses: 'Licenses',
  },
}
